﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovDeCaña : MonoBehaviour
{
    public float movHorizontal;
    public float movVertical;
    public int speed;

    void Update()
    {
        movHorizontal = Input.GetAxis("Horizontal1");
        movVertical = Input.GetAxis("Vertical1");
        transform.Translate(Vector3.right * movHorizontal * Time.deltaTime * speed);
        transform.Translate(Vector3.forward * movVertical * Time.deltaTime * speed);
    }

}

