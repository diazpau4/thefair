﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ZonaJMDDM : MonoBehaviour
{
    public int numeroEscena;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Player2")
        {
            SceneManager.LoadScene(numeroEscena);
        }
    }
}
