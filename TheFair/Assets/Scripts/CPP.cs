﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPP : MonoBehaviour
{
    public GameObject ObjPuntos;
    public GameObject ObjPuntos2;
    public int puntosQueDa;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball" )
        {
            ObjPuntos.GetComponent<PuntosPinball>().puntos += puntosQueDa;
        }

        if (other.tag == "Ball 2")
        {
            ObjPuntos2.GetComponent<PuntosPinball>().puntos += puntosQueDa;
        }
    }
}
