﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorGlobos : MonoBehaviour
{

    public GameObject GloboV;
    public GameObject GloboR;
    public GameObject[] spawns;
    public int i;
    public int j;


    void Start()
    {
        InvokeRepeating("GenerarGlobo", 0, 0.5f);
    }
    
    public void GenerarGlobo()
    {
        i = Random.Range(0, 2);
        j = Random.Range(0, 7);
        if (i == 0)
        {
            Instantiate(GloboV, spawns[j].transform.position, Quaternion.identity);
        }
        if (i == 1)
        {
            Instantiate(GloboR, spawns[j].transform.position, Quaternion.identity);
        }
        
    }

    void Update()
    {

        if(GloboV.GetComponent<Globos>().destroy)
        {
            Instantiate(GloboV, transform);
            GloboV.GetComponent<Globos>().destroy = false;
        }

        if (GloboR.GetComponent<Globos>().destroy)
        {
            Instantiate(GloboR, transform);
            GloboR.GetComponent<Globos>().destroy = false;
        }

        


    }
}
