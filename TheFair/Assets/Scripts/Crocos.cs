﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crocos : MonoBehaviour
{

    public GameObject Croco;
    public Rigidbody rb;
    private Vector3 startPos;
    private Quaternion startRot;



    public float velocidad;

    public void Start()
    {
        startPos = transform.position;
        startRot = transform.rotation;
    }

    void Update()
    {

        transform.Translate(Vector3.right * velocidad * Time.deltaTime);

    }

   
    public void RestorePosition()
    {
        transform.position = startPos;
        transform.rotation = startRot;
        RefreshRigidbody();

    }

    public void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void RefreshRigidbody()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }


}
