﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contador4P2 : MonoBehaviour
{

    public GameObject ObjPuntos;
    public GameObject OBJPrinci;
    public int puntosQueDa;

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player2")
        {
            ObjPuntos.GetComponent<Puntos4>().puntos += puntosQueDa;
            Destroy(OBJPrinci);
        }

    }
    
       
}
