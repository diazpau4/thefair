﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hammer2P : MonoBehaviour
{
    private Animator anim;

    public void Start()
    {
        anim = GetComponent<Animator>();

    }

    public void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            anim.SetBool("Golpeo", true);
        }

        if (Input.GetButtonUp("Fire1"))
        {
            anim.SetBool("Golpeo", false);
        }
    }
}
