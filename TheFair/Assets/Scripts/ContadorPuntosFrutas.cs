﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContadorPuntosFrutas : MonoBehaviour
{

    public GameObject ObjPuntos;
    public GameObject ObjPuntos2;
    public int puntosQueDa;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Reset" || other.tag == "Ball" || other.tag == "Bala")
        {
            ObjPuntos.GetComponent<PuntosFrutas>().puntos += puntosQueDa;
        }
        if (other.tag == "Reset" || other.tag == "Ball" || other.tag == "Bala")
        {
            ObjPuntos2.GetComponent<PuntosFrutas>().puntos += puntosQueDa;
        }

        if (other.tag == "Bombita")
        {
            ObjPuntos.GetComponent<PuntosFrutas>().puntos -= puntosQueDa;
        }
    }


}
