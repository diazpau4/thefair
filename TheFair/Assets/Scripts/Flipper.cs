﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour
{
    public Animator anim;
    public Animator anim2;

    void Awake()
    {
        anim = GetComponent<Animator>();
        anim2 = GetComponent<Animator>();
    }


    void Update()
    {
        FlipperD();
        FlipperI();
    }

    public void FlipperD()
    {
        if (Input.GetKey(KeyCode.D))
        {
            anim.SetBool("Activado", true);

        }
        else
        {
            anim.SetBool("Activado", false);
        }
    }

    public void FlipperI()
    {
        if (Input.GetKey(KeyCode.A))
        {
            anim2.SetBool("Activado1", true);
        }
        else 
        {
            anim2.SetBool("Activado1", false);
        }
    }
}
