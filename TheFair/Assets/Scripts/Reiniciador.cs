﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reiniciador : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Reset"))
        {
            other.GetComponent<Pelota>().RestorePosition();
        }
    }
}
