﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Puntos : MonoBehaviour
{
    public int puntos;
    public Text textoPuntos;
    

    private void Update()
    {
        textoPuntos.text = "SCORE: " + puntos.ToString() + "/5";
       
        if (puntos == 5)
        {
            SceneManager.LoadScene("GO");
        }
    }

}
