﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImpulsoDePelota : MonoBehaviour
{


    public int m_PlayerNumber = 1;
    public Rigidbody m_Shell;
    public Transform m_FireTransform;
    
    
    public float m_MinLaunchForce = 15f;
    public float m_MaxLaunchForce = 30f;
    public float m_MaxChargeTime = 0.75f;


    
    private float m_CurrentLaunchForce;
    private float m_ChargeSpeed;
    private bool m_Fired;


    private void OnEnable()
    {

        m_CurrentLaunchForce = m_MinLaunchForce;
        
    }


    private void Start()
    {

        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }


    private void Update()
    {

        


        if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
        {

            m_CurrentLaunchForce = m_MaxLaunchForce;
            Fire();
        }

        
        else if (Input.GetMouseButtonDown(0))
        {

            m_Fired = false;
            m_CurrentLaunchForce = m_MinLaunchForce;


         
        }

        else if (Input.GetMouseButtonDown(0) && !m_Fired)
        {

            m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

           
        }

        else if (Input.GetMouseButtonUp(0) && !m_Fired)
        {

            Fire();
        }
    }


    private void Fire()
    {

        m_Fired = true;


        Rigidbody shellInstance =
            Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;


        shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;


    


        m_CurrentLaunchForce = m_MinLaunchForce;
    }
}


  


