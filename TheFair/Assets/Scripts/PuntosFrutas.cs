﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuntosFrutas : MonoBehaviour
{
    public int puntos;
    public Text textoPuntos;

    private void Update()
    {
        textoPuntos.text = "SCORE: " + puntos.ToString() + "/40";

        if (puntos == 40)
        {
            SceneManager.LoadScene("GO");
        }
    }

}
