﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pescar : MonoBehaviour
{

    public GameObject ObjAPescar;

    public Transform PuntoMagnetico;


    private bool activo2;
    private bool agarrado;



    void Update()
    {


        if (activo2 == true)
        {
            if (Input.GetButton("Fire1"))
            {
                if (agarrado == false)
                {
                    ObjAPescar.transform.SetParent(PuntoMagnetico);
                    ObjAPescar.transform.position = PuntoMagnetico.position;
                    ObjAPescar.GetComponent<Rigidbody>().isKinematic = true;
                    agarrado = true;
                }

            }

            if (Input.GetButton("Fire1"))
            {
                if (agarrado == true)
                {
                    ObjAPescar.transform.SetParent(null);
                    ObjAPescar.GetComponent<Rigidbody>().isKinematic = false;
                    agarrado = false;
                }

            }
        }

       
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Iman")
        {

            activo2 = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Iman")
        {

            activo2 = false;
        }
    }
}