﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PONG : MonoBehaviour
{
    public bool isPlayer1;
    public float speed;
    public Rigidbody2D rb;
    public Vector3 startpos;
    private float movement;

    void Start()
    {
        startpos = transform.position;
    }

    
    void Update()
    {
        if (isPlayer1)
        {
            movement = Input.GetAxisRaw("Vertical1");

        }
        else
        {
            movement = Input.GetAxisRaw("JoyStick Y");
        }

        rb.velocity = new Vector2(rb.velocity.x, movement * speed);
    }

    public void Reset()
    {
        rb.velocity = Vector2.zero;
        transform.position = startpos;
    }
}
