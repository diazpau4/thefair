﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contador20 : MonoBehaviour
{
    public GameObject ObjPuntos;
    public GameObject ObjPuntos2;
    public int puntosQueDa;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Reset" || other.tag == "Ball" || other.tag == "Bala" || other.tag == "Hammer")
        {
            ObjPuntos.GetComponent<Puntos20>().puntos += puntosQueDa;
        }

        if (other.tag == "Reset" || other.tag == "Ball" || other.tag == "Bala" || other.tag == "Hammer")
        {
            ObjPuntos2.GetComponent<Puntos20>().puntos += puntosQueDa;
        }
    }
}
