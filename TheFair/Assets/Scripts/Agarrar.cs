﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarrar : MonoBehaviour
{
    public GameObject PelotaBasquet;
    public Transform ManoAgarra;
    public float fuerza;
    private bool activo;
    private Vector3 escala;
    private bool enMano;

    private void Start()
    {
        escala = PelotaBasquet.transform.localScale;
    }

    void Update()
    {
        if (activo == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                PelotaBasquet.transform.SetParent(ManoAgarra);
                PelotaBasquet.transform.position = ManoAgarra.position;
                PelotaBasquet.GetComponent<Rigidbody>().isKinematic = true;
                PelotaBasquet.transform.localRotation = ManoAgarra.rotation;
                enMano = true;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            PelotaBasquet.transform.SetParent(null);
            PelotaBasquet.GetComponent<Rigidbody>().isKinematic = false;
            PelotaBasquet.transform.localScale = escala;
            if (enMano == true)
            {
                PelotaBasquet.GetComponent<Rigidbody>().AddForce(transform.forward * fuerza, ForceMode.Impulse);
                enMano = false;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            PelotaBasquet.transform.SetParent(null);
            PelotaBasquet.GetComponent<Rigidbody>().isKinematic = false;
            PelotaBasquet.transform.localScale = escala;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            activo = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        activo = false;
    }
}
