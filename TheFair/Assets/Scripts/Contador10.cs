﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contador10 : MonoBehaviour
{

    public GameObject ObjPuntos;
    public int puntosQueDa;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Reset" || other.tag == "Ball" || other.tag == "Bala" || other.tag == "Hammer")
        {
            ObjPuntos.GetComponent<Puntos10>().puntos += puntosQueDa;
        }

       
    }

   


}
