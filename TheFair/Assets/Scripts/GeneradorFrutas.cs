﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorFrutas : MonoBehaviour
{
    public GameObject Manzana;
    public GameObject Pera;
    public GameObject Naranja;
    public GameObject Banana;
    public GameObject Bombita;
    public GameObject[] spawns;
    
    public int i;
    public int j;


    void Start()
    {
        InvokeRepeating("GenerarFruta", 0, 0.5f);
    }

    public void GenerarFruta()
    {
        i = Random.Range(0, 7);
        j = Random.Range(0, 9);
        if (i == 0)
        {
            Instantiate(Manzana, spawns[j].transform.position, Quaternion.identity);
        }
        if (i == 1)
        {
            Instantiate(Pera, spawns[j].transform.position, Quaternion.identity);
        }
        if (i == 2)
        {
            Instantiate(Naranja, spawns[j].transform.position, Quaternion.identity);
        }
        if (i == 3)
        {
            Instantiate(Banana, spawns[j].transform.position, Quaternion.identity);
        }
        if (i == 4)
        {
            Instantiate(Bombita, spawns[j].transform.position, Quaternion.identity);
        }
    }

    void Update()
    {

        if (Manzana.GetComponent<Frutas>().destroy)
        {
            Instantiate(Manzana, transform);
            Manzana.GetComponent<Frutas>().destroy = false;
        }

        if (Pera.GetComponent<Frutas>().destroy)
        {
            Instantiate(Pera, transform);
            Pera.GetComponent<Frutas>().destroy = false;
        }

        if (Naranja.GetComponent<Frutas>().destroy)
        {
            Instantiate(Naranja, transform);
            Naranja.GetComponent<Frutas>().destroy = false;
        }

        if (Banana.GetComponent<Frutas>().destroy)
        {
            Instantiate(Banana, transform);
            Banana.GetComponent<Frutas>().destroy = false;
        }

        if (Bombita.GetComponent<Frutas>().destroy)
        {
            Instantiate(Bombita, transform);
            Bombita.GetComponent<Frutas>().destroy = false;
        }

    }
}
