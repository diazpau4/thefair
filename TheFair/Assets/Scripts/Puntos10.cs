﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Puntos10 : MonoBehaviour
{
    public int puntos;
    public Text textoPuntos;

    private void Update()
    {
        textoPuntos.text = "SCORE: " + puntos.ToString() + "/10";

        if (puntos == 10)
        {
            SceneManager.LoadScene("GO");
        }
    }

}
