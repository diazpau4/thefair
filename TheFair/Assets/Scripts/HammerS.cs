﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerS : MonoBehaviour
{

    private Animator anim;

    public void Start()
    {
        anim = GetComponent<Animator>();

    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            anim.SetBool("Golpeo", true);
        }

        if (Input.GetMouseButtonUp(0))
        {
            anim.SetBool("Golpeo", false);
        }
    }


}
