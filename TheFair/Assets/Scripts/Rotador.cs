﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    public int speed;
    void Update()
    {
        transform.Rotate(new Vector3(0, 30, 0) * speed * Time.deltaTime);
    }
}

