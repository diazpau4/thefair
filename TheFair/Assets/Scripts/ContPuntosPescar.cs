﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContPuntosPescar : MonoBehaviour
{
    public GameObject ObjPuntos;
    public int puntosQueDa;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Reset" || other.tag == "Ball")
        {
            ObjPuntos.GetComponent<Puntos>().puntos += puntosQueDa;

            
        }
    }
}
