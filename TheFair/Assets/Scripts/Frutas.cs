﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frutas : MonoBehaviour
{
    public float velocidad;
    public bool destroy;

    private void Start()
    {
        destroy = false;
    }
    void Update()
    {
        transform.Translate(Vector3.down * velocidad * Time.deltaTime);

    }

    public void OnDestroy()
    {
        destroy = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Barra")
        {
            Destroy(gameObject);
        }
    }
}
