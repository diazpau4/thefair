﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globos : MonoBehaviour
{
    public GameObject GloboR;
    public GameObject GloboV;
    public float velocidad;
    public bool destroy;


    private void Start()
    {
        
    }
    void Update()
    {
        transform.Translate(Vector3.up * velocidad * Time.deltaTime);

    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Bala" || other.tag == "Barra")
        {
            Destroy(gameObject);
        }

    }

}

   
