﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReiniciadorFichaTejo : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Reset"))
        {
            other.GetComponent<FichaTejo>().RestorePosition();
        }
    }
}
