﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovLatPat : MonoBehaviour
{
    
    public float movVertical;

    public int speed;

    void Update()
    {
        
   
        
        movVertical = Input.GetAxis("JoyStick X");
        
        transform.Translate(Vector3.forward * movVertical * Time.deltaTime * speed);
    }

}
