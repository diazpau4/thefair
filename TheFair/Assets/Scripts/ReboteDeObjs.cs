﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReboteDeObjs : MonoBehaviour
{

    public Rigidbody rb;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
            rb.AddForce(transform.forward);
        }
        
    }

}
