﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovCañaMando : MonoBehaviour
{
    public float movHorizontal;
    public float movVertical;
    public int speed;

    void Update()
    {
        movHorizontal = Input.GetAxis("JoyStick X");
        movVertical = Input.GetAxis("JoyStick Y");
        transform.Translate(Vector3.right * movHorizontal * Time.deltaTime * speed);
        transform.Translate(Vector3.forward * movVertical * Time.deltaTime * speed);
    }
}
