﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPlayers : MonoBehaviour
{
    
    public void Player1()
    {
        SceneManager.LoadScene("MainScene1P");
    }

    
    public void Player2()
    {
        SceneManager.LoadScene("MainScene2P");
    }

    public void Back()
    {
        SceneManager.LoadScene("Menú");
    }
   
    
}
