﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Puntos7 : MonoBehaviour
{
    public int puntos;
    public Text textoPuntos;

    private void Update()
    {
        textoPuntos.text = "SCORE: " + puntos.ToString() + "/7";

        if (puntos == 7)
        {
            SceneManager.LoadScene("GO");
        }
    }
}
