﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Puntos1 : MonoBehaviour
{

    public int puntos;
    public Text textoPuntos;
    public GameObject paredon;
    private void Update()
    {
        textoPuntos.text = "SCORE: " + puntos.ToString() + "/1";

        if (puntos == 1)
        {
            Destroy(paredon);
        }
    }
}
