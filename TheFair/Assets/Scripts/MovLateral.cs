﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovLateral : MonoBehaviour
{
    public float movHorizontal;
    public int speed;
    void Start()
    {
        
    }

    
    void Update()
    {
        movHorizontal = Input.GetAxis("Horizontal1");
        transform.Translate(Vector3.right * movHorizontal * Time.deltaTime * speed);
    }
}
