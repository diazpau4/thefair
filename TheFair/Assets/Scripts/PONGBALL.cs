﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PONGBALL : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    public Vector3 starpos;
    void Start()
    {
        starpos = transform.position;
        Launch();
    }

    
    void Update()
    {
        
    }

    public void Reset()
    {
        rb.velocity = Vector2.zero;
        transform.position = starpos;
        Launch();
    }
    private void Launch()
    {
        float x = Random.Range(0, 2) == 0 ? -1 : 1;
        float y = Random.Range(0, 2) == 0 ? -1 : 1;
        rb.velocity = new Vector2(speed * x, speed * y);
    }

   
}
