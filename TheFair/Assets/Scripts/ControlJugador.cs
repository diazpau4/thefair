﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public Camera cam;
    public float speedH;
    public float speedV;
    float ejeV, ejeH;

    public float rotmax;
    public float rotMin;

    public float speedMov;

    CharacterController cc;

    Vector3 mov = Vector3.zero;

    private void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    private void Update()
    {
        ejeH = speedH * Input.GetAxis("Mouse X");
        ejeV += speedV * Input.GetAxis("Mouse Y");

        cam.transform.localEulerAngles = new Vector3(-ejeV, 0, 0);
        transform.Rotate(0, ejeH, 0);
        ejeV = Mathf.Clamp(ejeV, rotMin, rotmax);

        mov = new Vector3(Input.GetAxis("Horizontal1"), 0.0f, Input.GetAxis("Vertical1"));
        mov = transform.TransformDirection(mov) * speedMov;

        cc.Move(mov * Time.deltaTime);
        
    }






}
