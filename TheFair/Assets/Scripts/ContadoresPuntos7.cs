﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContadoresPuntos7 : MonoBehaviour
{
    public GameObject ObjPuntos;
    public int puntosQueDa;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Reset" || other.tag == "Ball" || other.tag == "Bala")
        {
            ObjPuntos.GetComponent<Puntos7>().puntos += puntosQueDa;
        }
    }
}
