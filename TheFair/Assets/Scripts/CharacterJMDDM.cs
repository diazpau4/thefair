﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJMDDM : MonoBehaviour
{
    public bool isPlayer1;
    public float movHorizontal;
    public float movVertical;
    public float speed;
    public Rigidbody rb;
    public Vector3 starpos;
    void Start()
    {
        starpos = transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemies")
        {
            transform.position = starpos;
        }
    }


    void Update()
    {
        if(isPlayer1)
        {
            movHorizontal = Input.GetAxis("Horizontal1");
            movVertical = Input.GetAxis("Vertical1");
            transform.Translate(Vector3.right * movHorizontal * Time.deltaTime * speed);
            transform.Translate(Vector3.forward * movVertical * Time.deltaTime * speed);
        }
        else
        {
            movHorizontal = Input.GetAxis("JoyStick X");
            movVertical = Input.GetAxis("JoyStick Y");
            transform.Translate(Vector3.right * movHorizontal * Time.deltaTime * speed);
            transform.Translate(Vector3.forward * movVertical * Time.deltaTime * speed);
        }
       
    }
}
