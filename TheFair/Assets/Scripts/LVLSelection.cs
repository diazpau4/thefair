﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LVLSelection : MonoBehaviour
{
    public void LVL1()
    {
        SceneManager.LoadScene("J10L1");
    }
    public void LVL2()
    {
        SceneManager.LoadScene("J10L2");

    }
    public void LVL3()
    {
        SceneManager.LoadScene("J10L3");
    }
    public void LVL12P()
    {
        SceneManager.LoadScene("J10L12P");
    }
    public void LVL22P()
    {
        SceneManager.LoadScene("J10L22P");
    }
    public void LVL32P()
    {
        SceneManager.LoadScene("J10L32P");
    }

    public void Back()
    {
        SceneManager.LoadScene("MainScene1P");
    }

    public void Back2()
    {
        SceneManager.LoadScene("MainScene2P");
    }

}
