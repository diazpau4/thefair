﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper2P : MonoBehaviour
{

    public Animator anim;
    public Animator anim2;

    void Awake()
    {
        anim = GetComponent<Animator>();
        anim2 = GetComponent<Animator>();
    }

    
    void Update()
    {
        FlipperD();
        FlipperI();
    }

    public void FlipperD()
    {
        if (Input.GetMouseButtonDown(1))
        {
            anim.SetBool("Activado", true);

        }
        else if (Input.GetMouseButtonUp(1))
        {
            anim.SetBool("Activado", false);
        }
    }

    public void FlipperI()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            anim2.SetBool("Activado1", true);
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            anim2.SetBool("Activado1", false);
        }
    }

}
