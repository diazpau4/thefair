﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorDePajaros : MonoBehaviour
{
    
    public GameObject pajaro;
    public GameObject[] spawns;
    public int i;
    public int j;


    void Start()
    {
        InvokeRepeating("GenerarGlobo", 0, 5);
    }

    public void GenerarGlobo()
    {
        i = Random.Range(0, 1);
        j = Random.Range(0, 1);
        if (i == 0)
        {
            Instantiate(pajaro, spawns[j].transform.position, Quaternion.identity);
        }

    }

    void Update()
    {

        if (pajaro.GetComponent<Pajaros>().destroy)
        {
            Instantiate(pajaro, transform);
            pajaro.GetComponent<Pajaros>().destroy = false;
        }



    }
}
