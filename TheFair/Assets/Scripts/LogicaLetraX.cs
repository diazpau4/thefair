﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaLetraX : MonoBehaviour
{

    public float velocidad;
    public int contador = 0;
    public bool adentro = false;


    void Update()
    {
        transform.position += transform.right * -velocidad * Time.deltaTime;

        if (contador == 2)
        {
            adentro = true;
        }
        else
        {
            adentro = false;
        }

        if (Input.GetButtonDown("Fire3"))
        {
            if (adentro)
            {
                GameObject.Find("CasillaJugador").GetComponent<LogicaJugadorMusical>().score++;
                GameObject.Find("CasillaJugador").GetComponent<LogicaJugadorMusical>().texto.text = "Score: " +
                GameObject.Find("CasillaJugador").GetComponent<LogicaJugadorMusical>().score.ToString();

                Destroy(gameObject);
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == ("Player"))
        {
            contador++;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == ("Player"))
        {
            contador--;
        }
    }
}
