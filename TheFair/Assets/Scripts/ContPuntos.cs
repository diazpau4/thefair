﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContPuntos : MonoBehaviour
{

    public GameObject ObjPuntos;
    public int puntosQueDa;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Reset" || other.tag == "Ball")
        {
            ObjPuntos.GetComponent<Puntos>().puntos += puntosQueDa;
        }
    }


}
