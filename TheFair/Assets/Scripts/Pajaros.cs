﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pajaros : MonoBehaviour
{
    public float velocidad;
    public bool destroy;

    void Update()
    {
        transform.Translate(Vector3.right * velocidad * Time.deltaTime);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Barra" || other.tag == "Bala")
        {
            Destroy(gameObject);
        }
        
    }
}
