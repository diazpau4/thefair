﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PescarAmarillo : MonoBehaviour
{

    public GameObject ObjAPescar;
    public Transform PuntoMagnetico2;


    private bool activo;




    void Update()
    {
        if (activo == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ObjAPescar.transform.SetParent(PuntoMagnetico2);
                ObjAPescar.transform.position = PuntoMagnetico2.position;
                ObjAPescar.GetComponent<Rigidbody>().isKinematic = true;
            }

            if (Input.GetMouseButtonUp(0))
            {
                ObjAPescar.transform.SetParent(null);
                ObjAPescar.GetComponent<Rigidbody>().isKinematic = false;
            }
        }

       


    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Iman2")
        {
            activo = true;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Iman2")
        {
            activo = false;

        }
    }


}
