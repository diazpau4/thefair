﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{

    public GameObject Bala;
    public Transform spawnPoint;

    public float fuerzaDeDisparo = 1500f;
    public float radioDeDisparo = 0.5f;

    private float RateTimeDeDisparo = 0;

    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            if (Time.time>RateTimeDeDisparo)
            {

                GameObject nuevaBala;

                nuevaBala = Instantiate(Bala, spawnPoint.position, spawnPoint.rotation);

                nuevaBala.GetComponent<Rigidbody>().AddForce(spawnPoint.forward * fuerzaDeDisparo);

                RateTimeDeDisparo = Time.time + radioDeDisparo;

                Destroy(nuevaBala, 2);

            }
        }

    }
}
