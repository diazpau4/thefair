﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FichaTejo : MonoBehaviour
{
    public float thrust = 1.0f;
    private Vector3 startPos;
    private Rigidbody rb;
    private Quaternion startRot;

    public void Start()
    {
        startPos = transform.position;
        startRot = transform.rotation;
        rb.AddForce(0, 0, thrust, ForceMode.Impulse);
    }
    public void RestorePosition()
    {
        transform.position = startPos;
        transform.rotation = startRot;
        RefreshRigidbody();

    }

    public void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void RefreshRigidbody()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

 
}
